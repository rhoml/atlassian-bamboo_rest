require 'puppet_x/bamboo/config'
require 'spec_helper'

describe Bamboo::Config do

  let(:bamboo_base_url) {'http://bamboo.atlassian.com'}
  let(:admin_username) {'admin'}
  let(:admin_password) {'admin'}
  let(:crowd_admin_username) {'crowd_admin'}
  let(:crowd_admin_password) {'crowd_admin'}
  let(:bamboo_base_url_and_credential) do
    {
      'bamboo_base_url'      => bamboo_base_url,
      'admin_username'       => admin_username,
      'admin_password'       => admin_password,
      'crowd_admin_username' => crowd_admin_username,
      'crowd_admin_password' => crowd_admin_password
    }
  end

  describe '.file_path' do
    specify 'should retrieve path to Puppet\'s configuration directory from the API' do
      Puppet.settings[:confdir] = '/puppet/is/somewhere/else'
      expect(Bamboo::Config.file_path).to eq('/puppet/is/somewhere/else/bamboo_rest.conf')
    end

    specify 'should cache the filename' do
      expect(Bamboo::Config.file_path).to be(Bamboo::Config.file_path)
    end
  end

  describe '.read_config' do
    specify 'should fail if file reading fails' do
      YAML.should_receive(:load_file).and_raise('file not found')
      expect{described_class.read_config }.to raise_error(Puppet::ParseError, /file not found/)
    end

    specify 'should raise an error if bamboo url is missing' do
      YAML.should_receive(:load_file).and_return(bamboo_base_url_and_credential.reject{|key, value| key == 'bamboo_base_url'})
      expect{ described_class.read_config }.to raise_error(Puppet::ParseError, /must contain a value for key 'bamboo_base_url'/)
    end

    specify 'should raise an error of admin username is missing' do
      YAML.should_receive(:load_file).and_return(bamboo_base_url_and_credential.reject{|key, value| key == 'admin_username'})
      expect{ described_class.read_config }.to raise_error(Puppet::ParseError, /must contain a value for key 'admin_username'/)
    end

    specify 'should raise an error of admin password is missing' do
      YAML.should_receive(:load_file).and_return(bamboo_base_url_and_credential.reject{|key, value| key == 'admin_password'})
      expect{ (described_class.read_config) }.to raise_error(Puppet::ParseError, /must contain a value for key 'admin_password'/)
    end

    specify 'should read bamboo base url' do
      YAML.should_receive(:load_file).and_return(bamboo_base_url_and_credential)
      expect(described_class.read_config[:bamboo_base_url]).to eq(bamboo_base_url)
    end

    specify 'should read admin username' do
      YAML.should_receive(:load_file).and_return(bamboo_base_url_and_credential)
      expect(described_class.read_config[:admin_username]).to eq(admin_username)
    end

    specify 'should read admin password' do
      YAML.should_receive(:load_file).and_return(bamboo_base_url_and_credential)
      expect(described_class.read_config[:admin_password]).to eq(admin_password)
    end

    specify 'should read admin user' do
      YAML.should_receive(:load_file).and_return(bamboo_base_url_and_credential)
      expect(described_class.read_config[:crowd_admin_username]).to eq(crowd_admin_username)
    end

    specify 'should read crowd admin password' do
      YAML.should_receive(:load_file).and_return(bamboo_base_url_and_credential)
      expect(described_class.read_config[:crowd_admin_password]).to eq(crowd_admin_password)
    end
  end

  describe '.get_user_repository_type' do
    specify 'should return the hibernate user repository type correctly' do
      File.stub(:file?).and_return(true)
      doc = REXML::Document.new "<atlassian-user><repositories><hibernate /></repositories></atlassian-user>"
      described_class.stub(:read_xml_file).and_return(doc)
      expect(described_class.get_user_repository_type("")).to eq(:hibernate)
    end

    specify 'should return the crowd user repository type correctly' do
      File.stub(:file?).and_return(true)
      doc = REXML::Document.new "<atlassian-user><repositories><crowd /></repositories></atlassian-user>"
      described_class.stub(:read_xml_file).and_return(doc)
      expect(described_class.get_user_repository_type("")).to eq(:crowd)
    end

    specify 'should return the hibernate user repository type if "atlassian-user.xml" does not exist' do
      File.stub(:file?).and_return(false)
      expect(described_class.get_user_repository_type("")).to eq(:hibernate)
    end
  end

  describe '.configure' do

    specify 'should use local username/password if bamboo config dir is not specified' do
      YAML.should_receive(:load_file).and_return(bamboo_base_url_and_credential)
      described_class.reset                         # make sure read_config method will be invoked
      described_class.configure do |url, config|
        expect(config[:admin_username]).to eq(admin_username)
        expect(config[:admin_password]).to eq(admin_password)
      end
    end

    specify 'should use local username/password if user repo type is hibernate' do
      bamboo_base_url_and_credential[:bamboo_home_dir] = "/opt/data"
      YAML.should_receive(:load_file).and_return(bamboo_base_url_and_credential)
      described_class.stub(:get_user_repository_type).and_return(:hibernate)
      described_class.reset                         # make sure read_config method will be invoked
      described_class.configure do |url, config|
        expect(config[:admin_username]).to eq(admin_username)
        expect(config[:admin_password]).to eq(admin_password)
      end
    end

    specify 'should use local username/password if user repo type is crowd' do
      bamboo_base_url_and_credential[:bamboo_home_dir] = "/opt/data"
      YAML.should_receive(:load_file).and_return(bamboo_base_url_and_credential)
      described_class.stub(:get_user_repository_type).and_return(:crowd)
      described_class.reset                         # make sure read_config method will be invoked
      described_class.configure do |url, config|
        expect(config[:admin_username]).to eq(crowd_admin_username)
        expect(config[:admin_password]).to eq(crowd_admin_password)
      end
    end
  end
end