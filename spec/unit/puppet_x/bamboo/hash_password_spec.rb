require 'puppet_x/bamboo/hash_password'
require 'spec_helper'

describe Bamboo::HashPassword do
  describe :check_password_hash do
    specify { expect(described_class.check_password_hash('{PKCS5S2}PYcs9fI17VC9rfSvOnoI2pzQqaDDtu5t/N97J2Iri6MMs2PcqlVknQRW/kCm4kNx','test')).to eq(true)}
  end

  describe :check_password_hash_failed do
    specify { expect(described_class.check_password_hash('{PKCS5S2}PYcs9fI17VC9rfSvOnoI2pzQqaDDtu5t/N97J2Iri6MMs2PcqlVknQRW/kCm4kNx','notTest')).to eq(false)}
  end

  describe :check_password_hash_first_password do
    specify { expect(described_class.check_password_hash('{PKCS5S2}PYcs9fI17VC9rfSvOnoI2pzQqaDDtu5t/N97J2Iri6MMs2PcqlVknQRW/kCm4kNx', nil)).to eq(false)}
  end

end