require 'spec_helper'

describe Puppet::Type.type(:bamboo_administration_general_configuration) do
  before :each do 
    @provider_class = described_class.provide(:simple) do
      mk_resource_methods
      def flush; end
      def self.instances; []; end
    end
    described_class.stubs(:defaultprovider).returns @provider_class
  end


  describe '#name' do
    specify 'should accept current' do
      expect { described_class.new(:name => 'current') }.to_not raise_error
    end

    specify 'should accept current' do
      expect { described_class.new(:name => :current) }.to_not raise_error
    end

    specify 'should not accept values other than current' do
      expect { described_class.new(:name => 'not_current') }.to raise_error
    end
  end

  describe :dashboard_page_size do
    specify 'should work withs positive numbers' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :dashboard_page_size => '30') }.to_not raise_error
    end

    specify 'should not accept 0' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :dashboard_page_size => 0) }.to raise_error
    end

    specify 'should not accept negative number' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :dashboard_page_size => '-1') }.to raise_error
    end

    specify 'should not accept non number' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :dashboard_page_size => 'test') }.to raise_error
    end
  end
  
  describe :branch_detection_interval do
    specify 'should work withs positive numbers' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :branch_detection_interval => '30') }.to_not raise_error
    end

    specify 'should not accept 0' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :branch_detection_interval => 0) }.to raise_error
    end

    specify 'should not accept negative number' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :branch_detection_interval => '-1') }.to raise_error
    end

    specify 'should not accept non number' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :branch_detection_interval => 'test') }.to raise_error
    end
  end

  describe :gravatar_enabled do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :gravatar_enabled => 'true')}.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :gravatar_enabled => 'false')}.to_not raise_error
    end

    specify 'should not accept non-boolean values' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :gravatar_enabled => 'abc')}.to raise_error
    end

    specify 'should not have default value' do
      expect (described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any')[:gravatar_enabled]).nil?
    end
  end

  describe :gzip_compression_enabled do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :gzip_compression_enabled => 'true')}.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :gzip_compression_enabled => 'false')}.to_not raise_error
    end

    specify 'should not accept non-boolean values' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :gzip_compression_enabled => 'abc')}.to raise_error
    end

    specify 'should not have default value' do
      expect (described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any')[:gzip_compression_enabled]).nil?
    end
  end

  describe :gravatar_server_url do
    specify 'should accept valid url' do
      expect { described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :gravatar_server_url => 'http://localhost:6990')}.to_not raise_error
    end

    specify 'should chomp a trailing \'/\'' do
      expect (described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any', :gravatar_server_url => 'http://localhost:6990/')[:gravatar_server_url]).should eql 'http://localhost:6990'
    end

    specify 'should not have default value' do
      expect (described_class.new(:name => 'current', :base_url => 'any', :instance_name => 'any')[:gravatar_server_url]).nil?
    end

  end
end
