require 'spec_helper'

describe Puppet::Type.type(:bamboo_upm_settings) do
  describe '#name' do
    it 'should only accept value true' do
      expect { described_class.new(:name => 'current', :marketplace_connection_enabled => 'true')}.to_not raise_error
      expect { described_class.new(:name => 'not_current', :marketplace_connection_enabled => 'true')}.to raise_error
    end
  end

  describe :marketplace_connection_enabled do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :marketplace_connection_enabled => 'true')}.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :marketplace_connection_enabled => 'false')}.to_not raise_error
    end

    specify 'should fail if passed a non-boolean variable' do
      expect { described_class.new(:name => 'current', :marketplace_connection_enabled => 'abcd')}.to raise_error
    end
  end

  describe :request_addon_enabled do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :request_addon_enabled => 'true')}.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :request_addon_enabled => 'false')}.to_not raise_error
    end

    specify 'should fail if passed a non-boolean variable' do
      expect { described_class.new(:name => 'current', :request_addon_enabled => 'abcd')}.to raise_error
    end
  end

  describe :email_notification_enabled do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :email_notification_enabled => 'true')}.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :email_notification_enabled => 'false')}.to_not raise_error
    end

    specify 'should fail if passed a non-boolean variable' do
      expect { described_class.new(:name => 'current', :email_notification_enabled => 'abcd')}.to raise_error
    end
  end

  describe :auto_update_enabled do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :auto_update_enabled => 'true')}.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :auto_update_enabled => 'false')}.to_not raise_error
    end

    specify 'should fail if passed a non-boolean variable' do
      expect { described_class.new(:name => 'current', :auto_update_enabled => 'abcd')}.to raise_error
    end
  end

  describe :private_listings_enabled do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :private_listings_enabled => 'true')}.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :private_listings_enabled => 'false')}.to_not raise_error
    end

    specify 'should fail if passed a non-boolean variable' do
      expect { described_class.new(:name => 'current', :private_listings_enabled => 'abcd')}.to raise_error
    end
  end
end