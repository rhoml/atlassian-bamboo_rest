require 'spec_helper'

describe Puppet::Type.type(:bamboo_build_concurrency) do
  describe '#name' do
    specify 'should not accept value other than current' do
      expect { described_class.new(:name => 'not_current', :build_concurrency_enabled => 'true')}.to raise_error
    end

    specify 'should accept current' do
      expect { described_class.new(:name => 'current', :build_concurrency_enabled => 'true')}.to_not raise_error
    end
  end
  describe :build_concurrency_enabled do
    specify 'should accept true' do
      expect { described_class.new(:name => 'current', :build_concurrency_enabled => 'true')}.to_not raise_error
    end

    specify 'should accept false' do
      expect { described_class.new(:name => 'current', :build_concurrency_enabled => 'false')}.to_not raise_error
    end

    specify 'should fail if passed a non-boolean variable' do
      expect { described_class.new(:name => 'current', :build_concurrency_enabled => 'abcd')}.to raise_error
    end
  end

  describe :default_concurrent_builds_number do
    specify 'should accept positive integer' do
      expect { described_class.new(:name => 'current', :build_concurrency_enabled => 'true', :default_concurrent_builds_number => 3)}.to_not raise_error
    end

    specify 'should not accept zero' do
      expect { described_class.new(:name => 'current', :build_concurrency_enabled => 'true', :default_concurrent_builds_number => 0)}.to raise_error
    end

    specify 'should not accept negative number' do
      expect { described_class.new(:name => 'current', :build_concurrency_enabled => 'true', :default_concurrent_builds_number => -1)}.to raise_error
    end

    specify 'should not accept non-digital values' do
      expect { described_class.new(:name => 'current', :build_concurrency_enabled => 'true', :default_concurrent_builds_number => 'abc')}.to raise_error
    end
  end
end