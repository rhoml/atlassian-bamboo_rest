require 'spec_helper'

describe Puppet::Type.type(:bamboo_role_permissions).provider(:ruby) do |variable|

  describe '#permissions' do
    specify 'should get empty permissions if bamboo returns nothing' do
      resource = {:name => 'ROLE_USER', :permissions => ['ACCESS'] }
      Bamboo::Rest.should_receive(:get_bamboo_settings).with("/rest/admin/latest/permissions/roles?name=#{resource[:name]}").and_return([])
      provider = described_class.new
      provider.resource = resource
      perms = provider.permissions
      expect(perms == []).to eq(true)
    end

    specify 'should get the correct permissions from bamboo' do
      bamboo_response = [ { 'name'        => 'ROLE_USER', 'permissions' => ['ACCESS', 'CREATE_PLAN'] } ]
      resource = {:name => 'ROLE_USER', :permissions => ['ACCESS'] }
      Bamboo::Rest.should_receive(:get_bamboo_settings).with("/rest/admin/latest/permissions/roles?name=#{resource[:name]}").and_return(bamboo_response)
      provider = described_class.new
      provider.resource = resource
      perms = provider.permissions
      expect(perms == %w(ACCESS CREATE_PLAN)).to eq(true)
    end
  end

  describe '#map_resource_to_role_permissions_config' do
    specify 'should map puppet resource to the correct json object' do
      resource = {:name => 'ROLE_USER', :permissions => ['ACCESS'] }
      provider = described_class.new
      provider.resource = resource
      result = provider.map_resource_to_role_permissions_config()
      expect(result['name']).to eq('ROLE_USER')
      expect(result['permissions']).to eq(['ACCESS'])
    end
  end

  describe '#flush' do
    specify 'should invoke the bamboo REST endpoint' do
      json = { 'name' => 'ROLE_USER', 'permissions' => %w(ACCESS, CREATE_PLAN)}
      provider = described_class.new
      provider.should_receive(:map_resource_to_role_permissions_config).and_return(json)
      Bamboo::Rest.should_receive(:update_bamboo_settings).with('/rest/admin/latest/permissions/roles', json, :post)
      provider.flush
    end
  end
end