require 'spec_helper'

type_class = Puppet::Type.type(:bamboo_agent)

describe Puppet::Type.type(:bamboo_agent).provider(:ruby) do

  def gen_agent_status(status)
    results = status.collect do |id, type, enabled|
      {
        "id"      => id,
        "type"    => type,
        "enabled" => enabled
      }
    end
    {'results' => results}
  end

  describe '.instances' do
    specify 'should generate the right resource config if no agents in bamboo' do
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return({ "results" => []})
      instances = described_class.instances()
      local_config = instances.find {|agent| agent.name == :local}
      remote_config = instances.find {|agent| agent.name == :remote }
      elastic_config = instances.find {|agent| agent.name == :elastic}
      expect(local_config.enabled).to eq :false
      expect(remote_config.enabled).to eq :false
      expect(elastic_config.enabled).to eq :false
    end

    specify 'should generate the right resource config if local agents are partially enabled' do
      agent_status = gen_agent_status([[1, 'local', true],[2, 'local', false],[3, 'remote', false]])
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return(agent_status)
      instances = described_class.instances()
      local_config = instances.find {|agent| agent.name == :local}
      remote_config = instances.find {|agent| agent.name == :remote }
      elastic_config = instances.find {|agent| agent.name == :elastic}
      expect(local_config.enabled).to eq :true
      expect(remote_config.enabled).to eq :false
      expect(elastic_config.enabled).to eq :false
    end

    specify 'should generate the right resource config if local agents are all disabled' do
      agent_status = gen_agent_status([[1, 'local', false],[2, 'local', false],[3, 'remote', false]])
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return(agent_status)
      instances = described_class.instances()
      local_config = instances.find {|agent| agent.name == :local}
      remote_config = instances.find {|agent| agent.name == :remote }
      elastic_config = instances.find {|agent| agent.name == :elastic}
      expect(local_config.enabled).to eq :false
      expect(remote_config.enabled).to eq :false
      expect(elastic_config.enabled).to eq :false
    end

    specify 'should generate the right resource config if remote agents are partially enabled' do
      agent_status = gen_agent_status([[1, 'remote', true],[2, 'local', true],[3, 'remote', false]])
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return(agent_status)
      instances = described_class.instances()
      local_config = instances.find {|agent| agent.name == :local}
      remote_config = instances.find {|agent| agent.name == :remote }
      elastic_config = instances.find {|agent| agent.name == :elastic}
      expect(local_config.enabled).to eq :true
      expect(remote_config.enabled).to eq :true
      expect(elastic_config.enabled).to eq :false
    end

    specify 'should generate the right resource config if remote agents are all disabled' do
      agent_status = gen_agent_status([[1, 'remote', false],[2, 'local', false],[3, 'remote', false]])
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return(agent_status)
      instances = described_class.instances()
      local_config = instances.find {|agent| agent.name == :local}
      remote_config = instances.find {|agent| agent.name == :remote }
      elastic_config = instances.find {|agent| agent.name == :elastic}
      expect(local_config.enabled).to eq :false
      expect(remote_config.enabled).to eq :false
      expect(elastic_config.enabled).to eq :false
    end

    specify 'should generate the right resource config if elastic agents are partially enabled' do
      agent_status = gen_agent_status([[1, 'elastic', true],[2, 'elastic', false],[3, 'remote', false]])
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return(agent_status)
      instances = described_class.instances()
      local_config = instances.find {|agent| agent.name == :local}
      remote_config = instances.find {|agent| agent.name == :remote }
      elastic_config = instances.find {|agent| agent.name == :elastic}
      expect(local_config.enabled).to eq :false
      expect(remote_config.enabled).to eq :false
      expect(elastic_config.enabled).to eq :true
    end

    specify 'should generate the right resource config if elastic agents are all disabled' do
      agent_status = gen_agent_status([[1, 'elastic', false],[2, 'elastic', false],[3, 'remote', false]])
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return(agent_status)
      instances = described_class.instances()
      local_config = instances.find {|agent| agent.name == :local}
      remote_config = instances.find {|agent| agent.name == :remote }
      elastic_config = instances.find {|agent| agent.name == :elastic}
      expect(local_config.enabled).to eq :false
      expect(remote_config.enabled).to eq :false
      expect(elastic_config.enabled).to eq :false
    end

  end

  describe '.prefetch' do
    specify 'should match the provide successfully' do
      provider = described_class.new(
        {
          :name    => 'local',
          :enabled => false,
        }
      )

      described_class.should_receive(:instances).and_return([provider])

      resource = type_class.new(
        {
          :name    => 'local',
          :enabled => true
        }
      )

      described_class.prefetch({'local' => resource})
      expect(resource.provider) == provider
    end
  end

  describe '#flush' do
    specify 'should be able to disable local agents' do
      agent_status = gen_agent_status([[1, 'local', true], [2, 'remote', true], [3, 'elastic', true]])
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return(agent_status)
      Bamboo::Rest.should_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/1', {'enabled' => false})
      Bamboo::Rest.should_not_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/2', :any_parameter)
      Bamboo::Rest.should_not_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/3', :any_parameter)
      provider = described_class.new()
      provider.resource = {:name => 'local', :enabled => false}
      provider.flush
    end

    specify 'should be able to enable local agents' do
      agent_status = gen_agent_status([[1, 'local', false], [2, 'local', false], [3, 'remote', false]])
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return(agent_status)
      Bamboo::Rest.should_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/1', {'enabled' => true})
      Bamboo::Rest.should_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/2', {'enabled' => true})
      Bamboo::Rest.should_not_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/3', :any_parameter)
      provider = described_class.new()
      provider.resource = {:name => 'local', :enabled => true}
      provider.flush
    end

    specify 'should be able to disable remote agents' do
      agent_status = gen_agent_status([[1, 'remote', true], [2, 'local', true], [3, 'elastic', true]])
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return(agent_status)
      Bamboo::Rest.should_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/1', {'enabled' => false})
      Bamboo::Rest.should_not_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/2', :any_parameter)
      Bamboo::Rest.should_not_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/3', :any_parameter)
      provider = described_class.new()
      provider.resource = {:name => 'remote', :enabled => false}
      provider.flush
    end

    specify 'should be able to enable remote agents' do
      agent_status = gen_agent_status([[1, 'local', false], [2, 'remote', false], [3, 'remote', false]])
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return(agent_status)
      Bamboo::Rest.should_not_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/1', :any_parameter)
      Bamboo::Rest.should_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/2', {'enabled' => true})
      Bamboo::Rest.should_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/3', {'enabled' => true})
      provider = described_class.new()
      provider.resource = {:name => 'remote', :enabled => true}
      provider.flush
    end

    specify 'should be able to disable elastic agents' do
      agent_status = gen_agent_status([[1, 'elastic', true], [2, 'elastic', true], [3, 'remote', true]])
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return(agent_status)
      Bamboo::Rest.should_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/1', {'enabled' => false})
      Bamboo::Rest.should_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/2', {'enabled' => false})
      Bamboo::Rest.should_not_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/3', :any_parameter)
      provider = described_class.new()
      provider.resource = {:name => 'elastic', :enabled => false}
      provider.flush
    end

    specify 'should be able to enable elastic agents' do
      agent_status = gen_agent_status([[1, 'elastic', false], [2, 'local', false], [3, 'remote', false]])
      Bamboo::Rest.should_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return(agent_status)
      Bamboo::Rest.should_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/1', {'enabled' => true})
      Bamboo::Rest.should_not_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/2', :any_parameter)
      Bamboo::Rest.should_not_receive(:update_bamboo_settings).with('/rest/admin/latest/config/agents/3', :any_parameter)
      provider = described_class.new()
      provider.resource = {:name => 'elastic', :enabled => true}
      provider.flush
    end

    specify 'should not send any http request if there is not any agent' do
      Bamboo::Rest.should_not_receive(:get_bamboo_settings).with('/rest/admin/latest/config/agents').and_return({"results" =>[]})
      Bamboo::Rest.should_not_receive(:update_bamboo_settings).with(:any_parameters)
      provider = described_class.new()
      provider.resource = {:name => 'local', :enabled => true}
      provider.flush
    end
  end
end