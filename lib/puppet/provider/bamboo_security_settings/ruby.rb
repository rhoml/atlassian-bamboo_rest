require 'json'
require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))

Puppet::Type.type(:bamboo_security_settings).provide(:ruby, :parent => Puppet::Provider::BambooProvider) do
  desc 'Puppet provider to manage security settings'

  # REST endpoint to manage bamboo security settings
  def self.bamboo_resource
    '/rest/admin/latest/security/settings'
  end

  def self.map_config_to_resource_hash(bamboo_security_settings)
    res_hash = {}

    res_hash[:read_only_external_user_management] = bamboo_security_settings['readOnlyExternalUserManagement'].to_s.intern unless bamboo_security_settings['readOnlyExternalUserManagement'].nil?
    res_hash[:sign_up_enabled] = bamboo_security_settings['signUp']['enabled'].to_s.intern unless bamboo_security_settings['signUp']['enabled'].nil?
    res_hash[:sign_up_enabled_captcha] = bamboo_security_settings['signUp']['enabledCaptcha'].to_s.intern unless bamboo_security_settings['signUp']['enabledCaptcha'].nil?
    res_hash[:display_contact_details_enabled] = bamboo_security_settings['displayContactDetailsEnabled'].to_s.intern unless bamboo_security_settings['displayContactDetailsEnabled'].nil?
    res_hash[:restricted_administrator_role_enabled] = bamboo_security_settings['restrictedAdministratorRoleEnabled'].to_s.intern unless bamboo_security_settings['restrictedAdministratorRoleEnabled'].nil?
    res_hash[:brute_force_protection_enabled] = bamboo_security_settings['bruteForceProtection']['enabled'].to_s.intern unless bamboo_security_settings['bruteForceProtection']['enabled'].nil?
    res_hash[:brute_force_protection_login_attempts] = bamboo_security_settings['bruteForceProtection']['loginAttempts'] unless bamboo_security_settings['bruteForceProtection']['loginAttempts'].nil?
    res_hash[:xsrf_protection_enabled] = bamboo_security_settings['xsrfProtection']['enabled'].to_s.intern unless bamboo_security_settings['xsrfProtection']['enabled'].nil?
    res_hash[:xsrf_protection_disable_for_http_get] = bamboo_security_settings['xsrfProtection']['disableForHTTPGET'].to_s.intern unless bamboo_security_settings['xsrfProtection']['disableForHTTPGET'].nil?
    res_hash[:resolve_artifacts_content_type_by_extension_enabled] = bamboo_security_settings['resolveArtifactsContentTypeByExtensionEnabled'].to_s.intern unless bamboo_security_settings['resolveArtifactsContentTypeByExtensionEnabled'].nil?
    res_hash[:sox_compliance_mode_enabled] = bamboo_security_settings['soxComplianceModeEnabled'].to_s.intern unless bamboo_security_settings['soxComplianceModeEnabled'].nil?

    res_hash
  end

  def map_resource_hash_to_config
    config = {}

    config['readOnlyExternalUserManagement'] = resource[:read_only_external_user_management] unless resource[:read_only_external_user_management].nil?
    sign_up = {}
    sign_up['enabled'] = resource[:sign_up_enabled] unless resource[:sign_up_enabled].nil?
    sign_up['enabledCaptcha'] = resource[:sign_up_enabled_captcha] unless resource[:sign_up_enabled_captcha].nil?
    config['signUp'] = sign_up
    config['displayContactDetailsEnabled'] = resource[:display_contact_details_enabled] unless resource[:display_contact_details_enabled].nil?
    config['restrictedAdministratorRoleEnabled'] = resource[:restricted_administrator_role_enabled] unless resource[:restricted_administrator_role_enabled].nil?
    brute_force_protection = {}
    brute_force_protection['enabled'] = resource[:brute_force_protection_enabled] unless resource[:brute_force_protection_enabled].nil?
    brute_force_protection['loginAttempts'] = resource[:brute_force_protection_login_attempts] unless resource[:brute_force_protection_login_attempts].nil?
    config['bruteForceProtection'] = brute_force_protection
    xsrf_protection = {}
    xsrf_protection['enabled'] = resource[:xsrf_protection_enabled] unless resource[:xsrf_protection_enabled].nil?
    xsrf_protection['disableForHTTPGET'] = resource[:xsrf_protection_disable_for_http_get] unless resource[:xsrf_protection_disable_for_http_get].nil?
    config['xsrfProtection'] = xsrf_protection
    config['resolveArtifactsContentTypeByExtensionEnabled'] = resource[:resolve_artifacts_content_type_by_extension_enabled] unless resource[:resolve_artifacts_content_type_by_extension_enabled].nil?
    config['soxComplianceModeEnabled'] = resource[:sox_compliance_mode_enabled] unless resource[:sox_compliance_mode_enabled].nil?

    config
  end

  # Ask puppet to handle attribute getter/setter methods
  mk_resource_methods

end