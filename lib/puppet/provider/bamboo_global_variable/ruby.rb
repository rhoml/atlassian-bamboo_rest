require 'json'
require 'puppet'
require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_ensurable_provider.rb'))

Puppet::Type.type(:bamboo_global_variable).provide(:ruby, :parent => Puppet::Provider::BambooEnsurableProvider) do
  desc 'Puppet provider to manage bamboo global variables'

  def self.bamboo_resource
    '/rest/admin/latest/globalVariables/'
  end

  def self.map_config_to_resource_hash(global_variables_res)

    size = global_variables_res['globalVariables']['size']
    max_result = global_variables_res['globalVariables']['max-result']
    if !size.nil? and !max_result.nil? and max_result < size
      Puppet::warning('The result of retrieving all bamboo variables are paginationed, we should iterate the pages')
    end

    global_variables_res['globalVariables']['globalVariables'].collect do |variable|
      new (
        {
          :ensure => :present,
          :name   => variable['name'],
          :value  => variable['value'],
          :id     => variable['id'],
        }
      )
    end
  end

  mk_resource_methods

  def value=(val)
    @dirty_flag = true;
  end

  def name=(val)
    @dirty_flag = true
  end

  def map_resource_hash_to_config
    {
        'name'  => resource[:name],
        'value' => resource[:value],
    }
  end
end