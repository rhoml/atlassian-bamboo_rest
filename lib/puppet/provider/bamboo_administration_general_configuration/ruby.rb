require 'json'
require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))

Puppet::Type.type(:bamboo_administration_general_configuration).provide(:ruby, :parent => Puppet::Provider::BambooProvider) do
  desc  "Puppet provider to update bamboo admin general configuration"

  # REST endpoint to manage bamboo administration general configuration
  def self.bamboo_resource
    '/rest/admin/latest/config/general'
  end

  # Map json result from bamboo REST endpoint to resource properties
  def self.map_config_to_resource_hash(bamboo_settings)
    gravatar_server_url = bamboo_settings['gravatarServerUrl']
    unless gravatar_server_url.nil?
      gravatar_server_url = gravatar_server_url.chomp("/")
    end

    {
      :instance_name             => bamboo_settings['instanceName'],
      :base_url                  => bamboo_settings['baseUrl'].chomp("/"),
      :broker_url                => bamboo_settings['brokerUrl'],
      :broker_client_url         => bamboo_settings['brokerClientUrl'],
      :dashboard_page_size       => bamboo_settings['dashboardDefaultPageSize'],
      :branch_detection_interval => bamboo_settings['branchDetectionIntervalSeconds'],
      :gravatar_enabled          => bamboo_settings['enableGravatarSupport'].to_s.intern,
      :gravatar_server_url       => gravatar_server_url,
      :gzip_compression_enabled  => bamboo_settings['enableGzipCompression'].to_s.intern
    }
  end

  # Map resource properties to json accepted by bamboo REST endpoint
  def map_resource_hash_to_config
    config = {}
    config['instanceName'] = resource[:instance_name] unless resource[:instance_name].nil?
    config['baseUrl'] = resource[:base_url].chomp("/") unless resource[:base_url].nil?
    config['brokerUrl'] = resource[:broker_url] unless resource[:broker_url].nil?
    config['brokerClientUrl'] = resource[:broker_client_url] unless resource[:broker_client_url].nil?
    config['dashboardDefaultPageSize'] = resource[:dashboard_page_size] unless resource[:dashboard_page_size].nil?
    config['branchDetectionIntervalSeconds'] = resource[:branch_detection_interval] unless resource[:branch_detection_interval].nil?
    config['enableGravatarSupport'] = resource[:gravatar_enabled] unless resource[:gravatar_enabled].nil?
    config['gravatarServerUrl'] = resource[:gravatar_server_url].chomp("/") unless resource[:gravatar_server_url].nil?
    config['enableGzipCompression'] = resource[:gzip_compression_enabled] unless resource[:gzip_compression_enabled].nil?

    config
  end

  # Instead of creating getter and setter methods for each property, puppet can handle that automatically for us.
  mk_resource_methods

end
