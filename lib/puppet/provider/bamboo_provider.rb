require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'rest.rb'))

class Puppet::Provider::BambooProvider < Puppet::Provider
  desc 'Abstract provider for bamboo resources'

  # Bamboo REST resource, needs to be overwritten by child class
  def self.bamboo_resource
    '/rest/api/latest/info'
  end

  def self.content_type
    'json'
  end

  # Find resource information of this type via invoking bamboo REST api.
  # Map response from json fromat to resource properties
  # Invoked during `puppet resource`
  def self.instances
    bamboo_settings = Bamboo::Rest.get_bamboo_settings(self.bamboo_resource, self.content_type)
    hash = map_config_to_resource_hash(bamboo_settings)
    hash[:name] = 'current'
    [new(hash)]
  end

  # Fetch information of the resource type, invoked during `puppet apply` and `puppet agent`
  def self.prefetch(resources)
    settings = instances
    resources.keys.each do |name|
      if provider = settings.find { |setting| setting.name == name }
        resources[name].provider = provider
      end
    end
  end

  # map REST response to resource hash
  def self.map_config_to_resource_hash(bamboo_settings)
    notice('Need to be implemented')
  end

  # map resource hash to format accepted by REST resource
  def map_resource_hash_to_config
    notice('Need to be implemented')
  end

  # Instead of applying each attribute one by one, flush after all attributes have been updated
  def flush
    config = map_resource_hash_to_config
    Bamboo::Rest.update_bamboo_settings(self.class.bamboo_resource, config, :put, self.class.content_type)
  end
end