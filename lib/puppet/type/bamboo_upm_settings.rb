require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

Puppet::Type.newtype(:bamboo_upm_settings)  do
  @doc = 'Puppet type for upm settings'

  newparam(:mame, :namevar => true) do
    desc 'Name of the configuration (i.e. current).'

    validate do |value|
      Util.assert_equal_to_current(value)
    end
  end

  newproperty(:marketplace_connection_enabled, :boolean => true) do
    desc 'Enables connection to marketplace. Warning: Disabling the connection prevents you from finding new add-ons or receiving free updates to your installed add-ons.'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:request_addon_enabled, :boolean => true) do
    desc 'You\'ll receive a notification whenever an end-user requests an add-on. View these requests by selecting the Most requested filter.'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:email_notification_enabled, :boolean => true) do
    desc 'Disabling this will prevent users and administrators from receiving add-on notifications via email.'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:auto_update_enabled, :boolean => true) do
    desc 'Add-on updates selected by Atlassian will be automatically downloaded and installed.'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:private_listings_enabled, :boolean => true) do
    desc 'Enable private listings to install and license add-ons that aren\'t publicly available.'

    newvalue(:true)
    newvalue(:false)
  end
end