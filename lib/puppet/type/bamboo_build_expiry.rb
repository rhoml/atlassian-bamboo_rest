require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

Puppet::Type.newtype(:bamboo_build_expiry) do
  @doc = 'Puppet type for build expiry settings'
  
  newparam(:name, :namevar => true) do
    desc 'Name of the configuration (i.e. current).'

    validate do |value|
      Util.assert_equal_to_current(value)
    end
  end

  newproperty(:duration) do
    desc 'Duration before expiry'

    validate do |value|
      raise ArgumentError, "Duration must be a non-negative integer, got #{value}" unless value.to_s =~ /\d+/
      raise ArgumentError, "Duration must be >= 0" unless value.to_i >= 0
    end
    munge { |value| Integer(value) }
  end

  newproperty(:period) do
    desc 'Period of the duration specified. Must be "days", "weeks", or "months"'
    
    newvalues(:days, :weeks, :months)
  end

  newproperty(:builds_to_keep) do
    desc 'How many builds will be kept'

    validate do |value|
      raise ArgumentError, "Number of builds to keep must be a non-negative number, got #{value}" unless value.to_s =~ /\d+/
      raise ArgumentError, "Number of builds to keep must be >= 0" unless value.to_i >= 0
    end 
    munge { |value| Integer(value) }
  end

  newproperty(:deployments_to_keep) do
    desc 'How many deployments will be kept'

    validate do |value|
      raise ArgumentError, "Number of deployments to keep must be a non-negative number, got #{value}" unless value.to_s =~ /\d+/
      raise ArgumentError, "Number of deployments to keep must be >= 2, 1 released and 1 rollback" unless value.to_i >= 2
    end 
    munge { |value| Integer(value) }
  end

  newproperty(:maximum_ignored_log_size) do
    desc 'Maximum ignored log size'

    validate do |value|
      raise ArgumentError, "Maximum ignored log size must be an integer, got #{value}" unless value.to_s =~ /\d+/
    end 
    munge { |value| Integer(value) }
  end

  newproperty(:expire_results) do
    desc 'This will determine whether build results expire'
    
    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:expire_artifacts) do
    desc 'This will determine whether build artifacts expire'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:expire_logs) do
    desc 'This will determine whether build logs expire'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:labels_to_exclude) do
    desc 'Labels to be excluded. Builds with these labels will be kept'

    validate do |value|
      raise ArgumentError, "labels_to_exclude must be a string" unless value.instance_of? String
    end
  end

  newproperty(:cron_expression) do
    desc 'Cron expression: Bamboo will check for expired data using the global and plan override expiry configurations at intervals based on this cron expression'

    munge {|value| value.to_s}
  end

  validate do
    raise ArgumentError, "Must specify either Duration or Builds To Keep (and one of them must be > 0)" if (!self[:duration].nil? && !self[:builds_to_keep].nil? && self[:duration] == 0 && self[:builds_to_keep] == 0)
  end

end