require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

Puppet::Type.newtype(:bamboo_remote_agent_support) do
  @doc = 'Puppet Type for Bamboo Remote Agent Support'

  newparam(:name, :namevar => true) do
    desc 'name of the configuration. E.g., current'

    validate do |value|
      Util.assert_equal_to_current(value)
    end
  end

  newproperty(:remote_agents_supported, :boolean => true) do
    desc 'Whether remote agents are supported'

    newvalue(:true)
    newvalue(:false)
  end

end