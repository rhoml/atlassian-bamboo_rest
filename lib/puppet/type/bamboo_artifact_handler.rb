require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'hash_password.rb'))

Puppet::Type.newtype(:bamboo_artifact_handler) do
  desc 'Puppet type for server local artifact handler'

  newparam(:name, :namevar => true) do
    desc 'name of the artifact handler resource, e.g. current'

    validate do |value|
       raise ArgumentError, "Valid artifact handler name can only be server_local, bamboo_remote, agent_local or s3" unless ['server_local', 'bamboo_remote', 'agent_local', 's3'].include?(value)
    end
  end

  newproperty(:shared_artifacts_enabled) do
    desc 'whether this artifact handler is enabled for shared artifacts'
    newvalues(:true, :false)
  end

  newproperty(:non_shared_artifacts_enabled) do
    desc 'whether this handler is enabled for non shared artifacts'
    newvalues(:true, :false)
  end

  newproperty(:artifact_storage_location) do
    desc 'Artifact storage location on bamboo agents. Only available for agent local artifact handlers'
  end

  newproperty(:access_key_id) do
    desc 'Access key id'
  end

  newproperty(:secret_access_key) do
    desc 'Secret access key id'

    # Changes the comparison function
    def insync?(is)
      if(is == :absent and @should[0] != :absent)
        return false
      elsif(is != :absent and @should[0] == :absent)
        return false
      elsif(is == :absent and @should[0] == :absent)
        return true
      else
        Bamboo::HashPassword.check_password_hash(is, @should[0])
      end
    end

    #Log messages are changed so that the password does not appear in the logs.
    def change_to_s(currentvalue, newvalue)
      if currentvalue == :absent
        return 'Secret access key has been created'
      else
        return 'Secret access key has been changed'
      end
    end

    def is_to_s( currentvalue )
      return '[old secret access key hash redacted]'
    end

    def should_to_s( newvalue )
      return '[new secret access key hash redacted]'
    end

  end

  newproperty(:bucket_name) do
    desc 'Bucket name'
  end

  newproperty(:bucket_path) do
    desc 'Parent path in bucke'
  end

  newproperty(:max_artifact_file_count) do
    desc 'Maximum number of files per artifact'

    munge { |value| Integer(value) }
  end

  validate do
    raise ArgumentError, 'artifact_storage_location can only be set for agent local artifact handler' if !self['artifact_storage_location'].nil? && self['name'] != 'agent_local'
    s3_properties = %w(access_key_id secret_access_key bucket_name bucket_path max_artifact_file_count)
    raise ArgumentError, "Attributes #{s3_properties} are only available for s3 artifact handler" if s3_properties.any?{|value| !self[value].nil?} and self['name'] != 's3'
    file_count = self['max_artifact_file_count']
    raise ArgumentError, 'max_artifact_file_count can only be positive integers' if !file_count.nil? && !(file_count.to_s =~ /\A\d+\z/)
  end
end