require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'hash_password.rb'))

Puppet::Type.newtype(:bamboo_global_variable) do
  @doc = 'Puppet type for bamboo global variable'

  ensurable

  newparam(:name, :namevar => true) do
    desc 'Name of the variable'

    validate do |value|
      raise ArgumentError, "Global variable name can only contain number, alphabet, _ or ., got #{value}" unless value.to_s =~ /\A[a-zA-Z0-9_\.]+\z/
      raise ArgumentError, "Global variable name must not exceed 255 characters" unless value.to_s.length <= 255
    end
  end

  newproperty(:value) do
    desc 'Value of the variable'

    validate do |value|
      raise ArgumentError, "Global variable value must not exceed 4000 characters" unless value.to_s.length <= 4000
    end

    def insync?(is)
      if self.resource[:name].downcase.to_s =~ /password/
        Bamboo::HashPassword.check_password_hash(is, @should[0])
      else
        is == @should[0]
      end
    end

    #Log messages are changed so that the password does not appear in the logs.
    def change_to_s(currentvalue, newvalue)
      if self.resource[:name].downcase.to_s =~ /password/
        if currentvalue == :absent
          return "A password variable has been created"
        else
          return "The password variable has been changed"
        end
      else
        super
      end
    end

    def is_to_s( currentvalue )
      if self.resource[:name].downcase.to_s =~ /password/
        return '[old password variable hash redacted]'
      else
        return currentvalue
      end
    end

    def should_to_s( newvalue )
      if self.resource[:name].downcase.to_s =~ /password/
        return '[new password variable hash redacted]'
      else
        return newvalue
      end
    end

  end
end