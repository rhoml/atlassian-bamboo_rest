require 'puppet/property/list'

Puppet::Type.newtype(:bamboo_group) do
  @doc = 'Puppet type to manage groups in bamboo. Only supported property is group permissions atm.'

  ensurable

  newparam(:name, :namevar => true) do
    desc 'Name fo the group, can only include letters in lower case, digits, -, _, . and @'

    validate do |value|
      raise ArgumentError, "Group name should match regex /[a-z|@|_|-|\\.|0-9|-]+/, but is #{value}" unless value.to_s =~ /\A[a-z|@|_|-|\.|0-9|-]+\z/
    end
  end

  newproperty(:permissions, :array_matching => :all) do
    desc 'Permissions of a bamboo group. Should be an array of permissions, valid permissions include ACCESS, SOX_COMPLIANCE, CREATE_PLAN, CREATE_REPOSITORY, RESTRICTED_ADMIN and ADMIN'

    validate do |value|
      raise ArgumentError, "Valid permission names can only contain {ACCESS, CREATE_PLAN, CREATE_REPOSITORY, SOX_COMPLIANCE, RESTRICTED_ADMIN, ADMIN}" unless value =~ /\A(ACCESS|CREATE_PLAN|CREATE_REPOSITORY|SOX_COMPLIANCE|RESTRICTED_ADMIN|ADMIN)\z/
    end

    def insync?(is)
      return false unless is.length == @should.length
      is & @should == is
    end
  end
end