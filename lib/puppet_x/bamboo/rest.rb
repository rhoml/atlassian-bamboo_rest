require 'json'
require 'yaml'
require 'rest_client'
require File.expand_path(File.join(File.dirname(__FILE__),  'config.rb'))
require File.expand_path(File.join(File.dirname(__FILE__),  'server.rb'))
require File.expand_path(File.join(File.dirname(__FILE__),  'exception.rb'))

module Bamboo
  # The class to invoke bamboo REST endpoint to GET/PUT configurations
  class Rest
    def self.request
      Bamboo::Config.configure { |bamboo_base_url, options|
        bamboo = RestClient::Resource.new(
          bamboo_base_url,
          :user         => options[:admin_username],
          :password     => options[:admin_password],
          :timeout      => options[:connection_timeout],
          :open_timeout => options[:connection_open_timeout]
        )
        yield bamboo
      }
    end

    def self.service
      @service = init_service
    end

    def self.init_service
      Bamboo::Config.configure { |bamboo_base_url, options|
        client = Bamboo::Service::HealthCheckClient.new(options)
        service = Bamboo::Service.new(client, options)
        Bamboo::CachingService.new(service)
      }
    end

    # Retrieve configurations of bamboo via REST endpoint
    # Make sure bamboo is running before actually invoking the endpoint
    def self.get_bamboo_settings(resource_name, content_type=:json)
      service.ensure_running
      response = request{ |bamboo|
        begin
          bamboo[resource_name].get :accept => content_type
        rescue => e
          yield e if block_given?
          Bamboo::ExceptionHandler.process(e) { |msg|
            raise "Could not get #{resource_name}, #{e} "
          }
        end
      }

      begin
        JSON.parse(response)
      rescue => e
        raise "Could not parse the JSON response from Bamboo (url: , resource: #{resource_name}): #{e} (response: #{response})"
      end
    end

    # Update bamboo configurations via REST end point
    # Make sure bamboo is running before actually invoking the endpoint
    def self.update_bamboo_settings(resource_name, config, method = :put, content_type=:json)
      service.ensure_running
      request { |bamboo|
        begin
          if method == :put
            bamboo[resource_name].put JSON.generate(config), :accept => content_type, :content_type => content_type
          elsif method == :post
            bamboo[resource_name].post JSON.generate(config), :accept => content_type, :content_type => content_type
          else
            raise 'Invalid method given'
          end
        rescue => e
          Bamboo::ExceptionHandler.process(e) { |msg|
            raise "Could not update #{resource_name} at #{bamboo.url}: #{msg}"
          }
        end
      }
    end

    # Create a bamboo resource via REST end point
    # Make sure bamboo is running before actually invoking the endpoint
    def self.create_bamboo_resource(resource_name, config)
      service.ensure_running
      request { |bamboo|
        begin
          bamboo[resource_name].post JSON.generate(config), :accept => :json, :content_type => :json
        rescue => e
          Bamboo::ExceptionHandler.process(e) { |msg|
            raise "Could not create #{resource_name} at #{bamboo.url}: #{msg}"
          }
        end
      }
    end

    # Delete bamboo configurations via REST end point
    # Make sure bamboo is running before actually invoking the endpoint
    def self.delete_bamboo_resource(resource_name)
      service.ensure_running
      request { |bamboo|
        begin
          bamboo[resource_name].delete :accept => :json
        rescue RestClient::ResourceNotFound
          # Do nothing here, the resource has already been deleted
        rescue => e
          Bamboo::ExceptionHandler.process(e) { |msg|
            raise "Could not delete #{resource_name} from #{bamboo.url}: #{msg}"
          }
        end
      }
    end
  end
end
