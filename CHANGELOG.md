##2016-04-11 - Release 1.0.4

Adds support to enable Bamboo SOX Compliance mode. It requires Bamboo 5.11.0-m369 or later.

##2016-04-06 - Release 1.0.3

Adds support for CREATE_REPOSITORY permission. It requires Bamboo 5.11.0-m346 or later.

##2016-03-31 - Release 1.0.2

Adds support for SOX_COMPLIANCE permission. It requires Bamboo 5.11.0-m274 or later.

##2016-02-11 - Release 1.0.1

Fix issue:

 * Agent Local artifact handler provider does not work with a new bamboo 5.10 instance

##2016-01-25 - Release 1.0.0

1st GA version after Bamboo 5.10.0 is released, nothing new than 0.0.7

##2015-12-30 - Release 0.0.7

In this release the puppet provider forces some resources can only have title `current`, otherwise puppet run will fail.
Bamboo only has one entry for some REST resources, e.g. mail server, elastic configuration and user repositories, so
the provider uses title `current` when fetching them. If those resources are defined with title other than `current` in
your manifest, puppet will apply the resource every time.

The enforced types include:

 * `bamboo_administration_general_configuration`
 * `bamboo_audit_log`
 * `bamboo_build_concurrency`
 * `bamboo_build_expiry`
 * `bamboo_build_monitoring`
 * `bamboo_elastic_configuration`
 * `bamboo_im_server`
 * `bamboo_mail_server`
 * `bamboo_quarantine`
 * `bamboo_remote_agent_support`
 * `bamboo_security_settings`
 * `bamboo_user_repositories`

##2015-11-02 - Release 0.0.6

This release contains 1 bugfix. It requires Bamboo 5.10.0-m52 or later.

 * [issue-1](https://bitbucket.org/atlassian/atlassian-bamboo_rest/issues/1/bamboo_im_server-failed-with-undefined)

##2015-10-30 - Release 0.0.5

This release provides 1 new puppet types to manage bamboo. It requires Bamboo 5.10.0-m52 or later.

 * `bamboo_im_server`

##2015-10-07 - Release 0.0.4

This release provides 4 new puppet types to manage bamboo. It requires Bamboo 5.10.0-m33 or later.

 * `bamboo_artifact_handler`
 * `bamboo_elastic_configuration`
 * `bamboo_mail_server`
 * `bamboo_shared_credential`

##2015-09-22 - Release 0.0.3

This release provides 7 puppet types to manage bamboo. It requires Bamboo 5.10.0-m27 or later.

 * `bamboo_global_variable`
 * `bamboo_agent`
 * `bamboo_upm_settings`
 * `bamboo_group`
 * `bamboo_role_permissions`
 * `bamboo_user_repositories`
 * `bamboo_quarantine`

##2015-06-04 - Release 0.0.2

This release provides 6 puppet types to manage bamboo. It requires Bamboo 5.9.0 or later.

 * `bamboo_administration_general_configuration`
 * `bamboo_audit_log`
 * `bamboo_build_concurrency`
 * `bamboo_build_monitoring`
 * `bamboo_global_variable`
 * `bamboo_security_settings`
